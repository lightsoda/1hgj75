package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.addons.nape.FlxNapeSpace;
import flixel.addons.nape.FlxNapeSprite;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxColor;
import flixel.util.FlxSpriteUtil;
import haxe.xml.Fast;
import lime.Assets;
import nape.callbacks.CbEvent;
import nape.callbacks.InteractionCallback;
import nape.callbacks.InteractionListener;
import nape.callbacks.InteractionType;
import nape.constraint.DistanceJoint;
import nape.constraint.PivotJoint;
import nape.geom.Vec2;
import nape.phys.Material;
import openfl.display.Tilemap;

class PlayState extends FlxState
{
	var ogmo:FlxOgmoLoader;
	var angle:Float = 0;
	var mode:Int = C.MODE_BUILD;
	var modetext:FlxText;
	var blockPlacementSprite:FlxSprite;
	
	var ball:FlxNapeSprite;
	//var ballJoint:DistanceJoint;
	//var lineSprite:FlxSprite;
	
	override public function create():Void
	{
		super.create();
		
		//ogmo = new FlxOgmoLoader(AssetPaths.level01__oel);
		//add(ogmo.loadTilemap(AssetPaths.tiles__png));
		
		FlxNapeSpace.init();
		FlxNapeSpace.space.gravity.setxy(0, 250);
		FlxNapeSpace.createWalls(0, 0, C.SCREEN_WIDTH, C.SCREEN_HEIGHT);
		
		ball = new FlxNapeSprite(0, 0, AssetPaths.ball__png, false);
		ball.createCircularBody();
		ball.body.cbTypes.add(C.CB_BALL);
		add(ball);

		FlxNapeSpace.space.listeners.add(new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, C.CB_BALL, C.CB_BLOCK, onBallCollides));
		
		//ballJoint = new DistanceJoint(null, ball.body, Vec2.weak(), Vec2.weak(), 0, 2);
		
		//lineSprite = new FlxSprite();
		//lineSprite.makeGraphic(C.SCREEN_WIDTH, C.SCREEN_HEIGHT, FlxColor.TRANSPARENT);
		//add(lineSprite);
		
		modetext = new FlxText(4, 4, 0, "");
		add(modetext);
		
		blockPlacementSprite = new FlxSprite(0, 0, AssetPaths.block__png);
		add(blockPlacementSprite);
	}

	public function onBallCollides(callback:InteractionCallback)
	{
		var blockBody = callback.int2.castBody;
		if (blockBody != null) {
			var block:FlxNapeSprite = cast blockBody.userData.sprite;
			block.destroy();
			FlxG.camera.shake(0.01, 0.1);
		}
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		angle += FlxG.elapsed * 2;
		FlxG.camera.angle = Math.sin(angle);
		
		if (FlxG.keys.justPressed.R) {
			FlxG.switchState(new PlayState());
		}
		
		if (FlxG.keys.justPressed.E) {
			mode == C.MODE_BUILD ? mode = C.MODE_DESTROY : mode = C.MODE_BUILD;
		}
		
		switch(mode) {
			case C.MODE_BUILD:
				modetext.text = "Build mode - [E] to toggle\n[R] to reset";
				blockPlacementSprite.visible = true;
				updateBuildMode(elapsed);
			case C.MODE_DESTROY:
				modetext.text = "Destruction mode - [E] to toggle\n[R] to reset";
				blockPlacementSprite.visible = false;
				updateDestroyMode(elapsed);
		}
		
	}
	
	function updateDestroyMode(elapsed:Float) 
	{
		ball.revive();
		//ballJoint.anchor1.setxy(FlxG.mouse.x, FlxG.mouse.y);
		ball.setPosition(FlxG.mouse.x, FlxG.mouse.y);
		
		//lineSprite.visible = true;
		//FlxSpriteUtil.fill(lineSprite, FlxColor.TRANSPARENT);
		//FlxSpriteUtil.drawLine(lineSprite, ballJoint.anchor1.x, ballJoint.anchor1.y, ballJoint.anchor2.x, ballJoint.anchor2.y);
	}
	
	function updateBuildMode(elapsed:Float) 
	{
		ball.kill();
		//lineSprite.visible = false;
		
		var numTilesX:Int = Std.int(C.SCREEN_WIDTH / C.TILE_SIZE);
		var numTilesY:Int = Std.int(C.SCREEN_HEIGHT / C.TILE_SIZE);
		var bx:Int = Std.int((FlxG.mouse.x / C.SCREEN_WIDTH) * C.TILE_SIZE);
		var by:Int = Std.int((FlxG.mouse.y / C.SCREEN_HEIGHT) * C.TILE_SIZE);
		bx *= numTilesX;
		by *= numTilesY;
		
		blockPlacementSprite.setPosition(bx, by);
		
		if (FlxG.mouse.justPressed) {
			var napesprite:FlxNapeSprite = new FlxNapeSprite(bx + 8, by + 8, AssetPaths.block__png);
			napesprite.body.setShapeMaterials(Material.steel());
			napesprite.body.userData.sprite = napesprite;
			napesprite.body.cbTypes.add(C.CB_BLOCK);
			add(napesprite);
		}
	}
}