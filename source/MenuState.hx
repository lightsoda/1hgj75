package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;

class MenuState extends FlxState
{
	var angle:Float = 0;
	var logo:FlxSprite;
	var logoScale:Float = 1;
	
	override public function create():Void
	{
		super.create();
		
		FlxG.cameras.bgColor = 0xff35264e;
		
		logo = new FlxSprite(0, 0, AssetPaths.logo__png);
		add(logo.screenCenter());
		
		var seventyfive:FlxText = new FlxText(0, 0, 0, "75", 16);
		seventyfive.screenCenter();
		seventyfive.y += logo.height / 2;
		add(seventyfive);
		
		var text:FlxText = new FlxText(0, 0, 0, "Press [LMB] or [ENTER] to continue");
		text.screenCenter();
		text.y += logo.height;
		add(text);
		
		//#if flash
		//FlxG.sound.playMusic(AssetPaths.music__mp3, C.MUSIC_VOLUME);
		//#else
		//FlxG.sound.playMusic(AssetPaths.music__wav, C.MUSIC_VOLUME);
		//#end
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		angle += FlxG.elapsed * 2;
		FlxG.camera.angle = Math.sin(angle);
		
		logoScale = 0.5 + (0.25 * Math.cos(angle));
		logo.scale.set(logoScale, logoScale);
		
		if (FlxG.mouse.justPressed || FlxG.keys.justPressed.ENTER) {
			FlxG.switchState(new PlayState());
		}
	}
}
