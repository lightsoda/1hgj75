package;
import nape.callbacks.CbType;

/**
 * ...
 * @author lightsoda
 */
class C
{
	static public inline var TILE_SIZE:Int = 16;
	static public inline var SCREEN_WIDTH:Int = 320;
	static public inline var SCREEN_HEIGHT:Int = 240;
	static public inline var MUSIC_VOLUME:Float = 0;
	static public inline var MODE_BUILD:Int = 0;
	static public inline var MODE_DESTROY:Int = 1;
	
	static public var CB_BALL:CbType = new CbType();
	static public var CB_BLOCK:CbType = new CbType();
}